import { TodoContextProvider } from "modules/Todo/context/TodoContext";
import { TodoView } from "modules/Todo/View";

function App() {
  return (
    <div>
      <TodoContextProvider>
        <TodoView />
      </TodoContextProvider>
    </div>
  );
}

export default App;
